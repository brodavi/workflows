### 0 to MVP

You are: an entrepreneur with an idea, funding, and a business plan.

We collaborate on:
  - Identifying and researching your target market.
  - Clarifying and making explicit your intentions and proposed business value.
  - Conducting a competitive analysis.
  - Identifying your riskiest assumptions and creating tests around them.
  - Designing, releasing, and iterating on a minimum viable product.
